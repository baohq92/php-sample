<?php
    const USERNAME = '3rMG5vubwywRDfhj0f1Hh07EMehUDsL257ucBHF0';
    const PASSWORD = '';
    const BASE_URL = 'https://api.eber.co/v3/public/';

    // callUrl('point/adjust','post',array('email'=> 'leo@eber.co', 'create'=>true,'action'=>'add','points'=>1));

    function callUrl($url, $method = '', $data = array())
    {
      $ch = curl_init();
      $data['business_id'] = 9;

      switch ($method) {
          case 'get':
              if (!empty($data)) {
                  $params = array();
                  foreach ($data as $k => $v) {
                      if (is_array($v)) {
                          foreach ($v as $value) {
                              $params[] = $k.'[]='.urlencode($value);
                          }
                      } else {
                          $params[] = $k.'='.urlencode($v);
                      }
                  }
                  $url .= '?' . implode('&', $params);
              }
              break;
          case 'post':
              curl_setopt($ch, CURLOPT_POST, 1);
              curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
              break;
          case 'put':
              curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
              curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
              break;
          case 'delete':
              curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
              curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
              break;
      }

      curl_setopt($ch, CURLOPT_URL, BASE_URL . $url);
      curl_setopt($ch, CURLOPT_TIMEOUT, 60);
      curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT'] . ' / Shopify Eber');
      curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

      curl_setopt($ch, CURLOPT_USERPWD, USERNAME.':'.PASSWORD);
      curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLINFO_HEADER_OUT, true);

      $output       = curl_exec($ch);
      $header_size  = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
      $http_code    = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      $full_header  = curl_getinfo($ch, CURLINFO_HEADER_OUT);

      curl_close($ch);

      $content    = substr($output, $header_size);
      $returnData = json_decode($content, true);

      if ($http_code >= 400) {
        if(array_key_exists("error", $returnData)) {
          return array(
              'status'  => 'fail',
              'message' => $returnData['error']['email'][0],
              'data'    => []
          );
          // var_dump(array(
          //     'status'  => 'fail',
          //     'message' => $returnData['error']['email'][0],
          //     'data'    => []
          // ));
        }
      }

      return array(
          'status'  => 'success',
          'message' => '',
          'data'    => $returnData
      );
      // var_dump(array(
      //     'status'  => 'success',
      //     'message' => '',
      //     'data'    => $returnData
      // ));
    }

?>
